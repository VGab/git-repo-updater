# -*- coding: utf-8  -*-
#
# Copyright (C) 2011-2016 Ben Kurtovic <ben.kurtovic@gmail.com>
# Modified by Volker Gabler <v.gabler@tum.de> for randomness
# Released under the terms of the MIT/TUM License. See LICENSE for details.

"""
gitup: the git repository updater
"""

__author__ = "Ben Kurtovic, Volker Gabler"
__copyright__ = "Copyright (C) 2011-2016 Ben Kurtovic"
__license__ = "MIT License"
__version__ = "0.5.5"
__email__ = "v.gabler@tum.de"

COUNTER=dict(
    TOTAL=0,
    WARN=0,
    ERROR=0
)
