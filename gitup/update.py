# -*- coding: utf-8  -*-
#
# Copyright (C) 2011-2018 Ben Kurtovic <ben.kurtovic@gmail.com>
# Released under the terms of the MIT License. See LICENSE for details.

from __future__ import print_function, absolute_import
from glob import glob

import os
import shlex
import traceback

from colorama import Fore, Style
from git import RemoteReference as RemoteRef, Repo, exc, Actor
from git.util import RemoteProgress
from gitdb.exc import BadName as BadNameError

from gitup import COUNTER
from gitup.config import (get_default_config_path, customLogger,
                          BOLD, BLUE, YELLOW, GREEN, RED, RESET,
                          INDENT1, INDENT2,
                          LG
                          )

AUTHOR = Actor("gabler", "v.gabler@tum.de")
COMMITER = Actor("Autocommiter(gabler)", "v.gabler@tum.de")

all__ = ["update_bookmarks", "update_directories", "run_command"]

_cmd_str = "{i}{{indent}}{{cmd}} {f}{{obj.name}}:".format(i=INDENT2, f=BOLD)
_cmd_clean_str = "{i}{{indent}}{{cmd}}{{obj.name}}:".format(i=INDENT2)
_done_str = "{{:<40}}{}done".format(GREEN)
_up_to_date_str = "{{:<40}}{}up to date".format(BLUE)
_throw_str = "{0}\n{1}"
_repo_info_str = "=> Repo {r.working_dir}: {msg}"
_branch_info_str = "=> Repo {r.working_dir} @branch {b.name}: {msg}"
_remote_info_str = "=> Repo {r.working_dir} <-remote {b.name}: {msg}"
_LOG_INDENT = "\n" + " " * 16


def _get_path_from_repo(repo):
    return os.path.split(repo.working_dir)[1]


def _log_repo_info_str(repo, indent=INDENT1):
    LG.lgr.info("{fmt}{repo_dir_name}:".format(fmt=indent + BOLD,
                                           repo_dir_name=_get_path_from_repo(repo)))

def _log_submodule_info_str(submodule, indent=INDENT2):
    LG.lgr.info("{fmt}{s.name}:".format(fmt=indent + BOLD,s=submodule))


def _log_done(msg):
    LG.lgr.info(_done_str.format(msg))


def _log_up_to_date(msg):
    LG.lgr.info(_up_to_date_str.format(msg))


def _get_repo_log_str(repo, msg, branch=None, remote=None):
    if branch:
        return _branch_info_str.format(r=repo, b=branch, msg=msg)
    elif remote is not None:
        return _remote_info_str.format(r=repo, b=remote, msg=msg)
    else:
        return _repo_info_str.format(r=repo, msg=msg)


def _log_warn(cmd_msg, warn_msg, repo, branch=None, remote=None):
    COUNTER["WARN"] += 1
    LG.lgr.warning(_throw_str.format(cmd_msg, _get_repo_log_str(repo, warn_msg, branch, remote)))


def _log_error(cmd_msg, err_msg, repo, branch=None, ex=None, remote=None):
    COUNTER["ERROR"] += 1
    LG.lgr.error(_throw_str.format(cmd_msg, _get_repo_log_str(repo, err_msg, branch, remote)))
    if ex is not None:
        LG.lgr.critical("Exception content:\n{}".format(ex))


def _get_username():
    try:
        import pwd
        return pwd.getpwuid(os.getuid())[0]
    except (ImportError, AttributeError):
        return None


def _get_default_commit_msg():
    """
    Return a default string as a commit message that can be used in e.g. backup scripts
    """
    _base_format = "{:=^60}".format(" Auto-Commit service on {hostname} by {user} @ {time} ")
    try:
        import platform
        from datetime import datetime
        return _base_format.format(hostname=platform.node(), user=_get_username(), time=datetime.now())
    except (AttributeError, ImportError):
        return None


class _ProgressMonitor(RemoteProgress):
    """Displays relevant output during the fetching process."""

    def __init__(self):
        super(_ProgressMonitor, self).__init__()
        self._started = False

    def update(self, op_code, cur_count, max_count=None, message=''):
        """Called whenever progress changes. Overrides default behavior."""
        if op_code & (self.COMPRESSING | self.RECEIVING):
            cur_count = str(int(cur_count))
            if max_count:
                max_count = str(int(max_count))
            if op_code & self.BEGIN:
                print("\b, " if self._started else " (", end="")
                if not self._started:
                    self._started = True
            if op_code & self.END:
                end = ")"
            elif max_count:
                end = "\b" * (1 + len(cur_count) + len(max_count))
            else:
                end = "\b" * len(cur_count)
            if max_count:
                print("{0}/{1}".format(cur_count, max_count), end=end)
            else:
                print(str(cur_count), end=end)


def _fetch_remotes(remotes, prune, repo, indent=''):
    """Fetch a list of remotes, displaying progress info along the way."""

    def _get_name(ref):
        """Return the local name of a remote or tag reference."""
        return ref.remote_head if isinstance(ref, RemoteRef) else ref.name

    info = [("NEW_HEAD", "new branch", "new branches"),
            ("NEW_TAG", "new tag", "new tags"),
            ("FAST_FORWARD", "branch update", "branch updates")]
    errors = []
    for remote in remotes:
        cmd_msg = _cmd_str.format(indent=indent, obj=remote, cmd="Fetching")
        err_msg = ""
        results = []
        if not remote.config_reader.has_option("fetch"):
            _log_warn(cmd_msg, "skipped - no refspec configured for {}".format(remote.name), repo, remote=remote)
            continue
        try:
            results = remote.fetch(progress=_ProgressMonitor(), prune=prune)
        except AssertionError as ex:
            # Seems to be the result of a bug in GitPython,  This happens when git initiates an auto-gc during fetch:
            msg = "skipped - assertion Error. Fetching {} might have been successful.".format(remote.name)
            _log_error(cmd_msg, msg, repo, remote=remote, ex=ex)
            continue
        except Exception as ex:
            msg = "skipped - exception encountered"
            _log_error(cmd_msg, msg, repo, remote=remote, ex=ex)
            continue
        rlist = []
        for attr, singular, plural in info:
            names = [_get_name(res.ref)
                     for res in results if res.flags & getattr(res, attr)]
            if len(names) == 1:
                rlist.append("{f1}{k}{f2}: {c}".format(f1=GREEN, k=singular, f2=RESET, c=names[0]))
            elif len(names) > 1:
                rlist.append("{f1}{k}{f2}:({c})".format(f1=GREEN, k=plural, f2=RESET, c=", ".join(names)))
        if rlist:
            separator =  _LOG_INDENT + INDENT1 * 4
            LG.lgr.info(cmd_msg + separator.join([" new updates found:"] + rlist))
        else:
            _log_up_to_date(cmd_msg)
    return errors


def _commit_state(repo, branch, commit_msg=None, indent=''):
    commit_msg = _get_default_commit_msg() if commit_msg is None else commit_msg
    cmd_msg = _cmd_str.format(indent=indent, obj=branch, cmd_msg="Committing")
    try:
        cf_reader = repo.config_reader()
        author = Actor(cf_reader.get_value("user", "name"), cf_reader.get_value("user", "email"))
    except (KeyError, AttributeError):
        _log_error(cmd_msg, "failed to execute:\t{}".format(commit_msg),
                   repo, branch)
        return False
    try:
        index = repo.index
        if repo.untracked_files:
            _log_warn(cmd_msg, "Git contains untracked files!", repo, branch)
        if repo.index.diff(None) or repo.index.diff("HEAD"):
            repo.git.add(update=True)
            index.commit(commit_msg, author=author, committer=author)
            _log_done(cmd_msg)
            return True
        else:
            _log_up_to_date(cmd_msg)
    except exc.GitCommandError as err:
        LG.lgr.exception(err.stderr)
    except Exception as e:
        LG.lgr.exception("Error while commiting {}:\n{}".format(branch, traceback.format_exc()))
    finally:
        return False


def _push_repo(repo, branch, enable=True, indent=''):
    cmd_msg = _cmd_str.format(obj=branch, indent=indent, cmd="Pushing")
    if enable:
        try:
            repo.git.push()
            _log_done(cmd_msg)
        except exc.GitCommandError as err:
            _log_error(cmd_msg + "->Error during Push: ", repo, branch, err)
    else:
        _log_up_to_date(cmd_msg)


def _update_branch(repo, branch, is_active=False, commit=False,
                   commit_msg=None, push=False, indent=''):
    """Update a single branch."""
    cmd_msg = _cmd_str.format(obj=branch, indent=indent, cmd="Updating")
    if is_active:
        if commit or push:
            _commit_state(repo, branch, commit_msg, indent)
    upstream = branch.tracking_branch()
    if not upstream:
        _log_warn(cmd_msg, "skipped - no upstream is tracked", repo, branch)
        return
    try:
        branch.commit
    except ValueError:
        _log_error(cmd_msg, "skipped - branch has no revisions", repo, branch)
        return
    try:
        upstream.commit
    except ValueError:
        _log_error(cmd_msg, "skipped - upstream does not exist", repo, branch)
        return
    except Exception as e:
        _log_error(cmd_msg, "skipped - unexpected Exception:", repo, branch, e)
        return
    try:
        base = repo.git.merge_base(branch.commit, upstream.commit)
    except exc.GitCommandError:
        _log_error(cmd_msg, "skipped - can't find merge base with upstream", repo, branch)
        return

    if repo.commit(base) == upstream.commit:
        _log_up_to_date(cmd_msg)
    else:
        if is_active:
            try:
                repo.git.merge(upstream.name, ff_only=True)
                _log_done(cmd_msg)
            except exc.GitCommandError as e:
                msg = e.stderr
                if "local changes" in msg and "would be overwritten" in msg:
                    info_str = "skipped - uncommitted changes"
                else:
                    info_str = "skipped - not possible to fast-forward"
                _log_error(cmd_msg, info_str, repo, branch)
                return
        else:
            status = repo.git.merge_base(
                branch.commit, upstream.commit, is_ancestor=True,
                with_extended_output=True, with_exceptions=False)[0]
            if status != 0:
                _log_error(cmd_msg, "skipped - not possible to fast-forward", repo, branch)
                return
            repo.git.branch(branch.name, upstream.name, force=True)
            _log_done(cmd_msg)
    if push and branch.commit.hexsha != upstream.commit.hexsha:
        _push_repo(repo, branch, enable=True, indent=indent)


def _update_repository(repo, current_only, fetch_only, prune,
                       commit=False, commit_msg=None, push=False,
                       update_submods=False, indent=''):
    """Update a single git repository by fetching remotes and rebasing/merging.

    The specific actions depend on the arguments given. We will fetch all
    remotes if *args.current_only* is ``False``, or only the remote tracked by
    the current branch if ``True``. If *args.fetch_only* is ``False``, we will
    also update all fast-forwardable branches that are tracking valid
    upstreams. If *args.prune* is ``True``, remote-tracking branches that no
    longer exist on their remote after fetching will be deleted.
    """
    COUNTER["TOTAL"] += 1
    _log_repo_info_str(repo, indent=indent)
    try:
        active = repo.active_branch
    except (TypeError, ValueError):  # Happens when HEAD is detached
        active = None
    if current_only:
        if not active:
            _log_warn("", "skipping: --current-only doesn't make sense with a detached HEAD.", repo, active)
            return
        ref = active.tracking_branch()
        if not ref:
            _log_error("", "skipping - no remote tracked by current branch.", repo, active)
            return
        remotes = [repo.remotes[ref.remote_name]]
    else:
        remotes = repo.remotes

    try:
        if not remotes:
            _log_error("", "skipping - no remotes configured to fetch", repo)
            return
        _fetch_remotes(remotes, prune, repo, indent)

        if not fetch_only:
            for branch in sorted(repo.heads, key=lambda b: b.name):
                _update_branch(repo, branch, branch == active,
                               commit=commit, push=push,
                               commit_msg=commit_msg, indent=indent)
        if update_submods:
            try:
                if repo.submodules:
                    LG.lgr.info(INDENT2 + "Processing submodule...")
                    for submodule in repo.submodules:
                        try:
                            sub_repo = submodule.module()
                            try:
                                submodule.update(init=True, recursive=True)
                            except exc.GitCommandError as e:
                                _log_error("",
                                           "updating submodule at {} failed".format(_get_path_from_repo(sub_repo)),
                                           repo, submodule, e)
                                continue
                            _update_repository(sub_repo, current_only,
                                               fetch_only, prune,
                                               commit=False, commit_msg=None, push=False,
                                               update_submods=False, indent=INDENT1 + INDENT2)
                        except (IOError, OSError) as e:
                            _log_error("", "IO or OS- Error: ", submodule, ex=e)
                        except Exception as e:
                            _log_error("", "Unexpected exception encountered: ", submodule, ex=e)
                    LG.lgr.info(INDENT2 + "... finished processing submodules!")
            except BadNameError as e:
                _log_error("", "Repo falsely configured: ", repo, ex=e)
            except exc.InvalidGitRepositoryError as e:
                _log_error("", "Invalid Submodules Configuration: ", repo, ex=e)
    except Exception as e:
        _log_error("", "Unexpected exception encountered: ", repo, ex=e)


def _run_command(repo, command):
    """Run an arbitrary shell command on the given repository."""
    _log_repo_info_str(repo)
    cmd = shlex.split(command)
    try:
        _, s_out, s_err = repo.git.execute(cmd, with_extended_output=True, with_exceptions=False)
    except exc.GitCommandNotFound as e:
        _log_error(cmd, "", repo, None, e)
        return
    if s_out:
        LG.lgr.info(_LOG_INDENT.join([INDENT2 + x for x in s_out.splitlines()]))
    if s_err:
        LG.lgr.error(_LOG_INDENT.join([INDENT2 + x for x in s_err.splitlines()]))


def _dispatch(base_path, callback, *args, **kwargs):
    """Apply a callback function on each valid repo in the given path.

    Determine whether the directory is a git repo on its own, a directory of
    git repositories, a shell glob pattern, or something invalid. If the first,
    apply the callback on it; if the second or third, apply the callback on all
    repositories contained within; if the last, print an error.

    The given args are passed directly to the callback function after the repo.
    """

    def _collect(paths, max_depth):
        """Return all valid repo paths in the given paths, recursively."""
        if max_depth == 0:
            return []

        valid = []
        for path in paths:
            try:
                Repo(str(path))
                valid.append(path)
            except exc.InvalidGitRepositoryError:
                if not os.path.isdir(path):
                    continue
                children = [os.path.join(path, it) for it in os.listdir(path)]
                valid += _collect(children, max_depth - 1)
            except exc.NoSuchPathError:
                continue
            except UnicodeEncodeError:
                LG.lgr.error('decoding issue in path ' + u'{0}'.format(path))
            except PermissionError:
                LG.lgr.error("you have no writing permission in path {}".format(path))
        return valid

    def _get_basename(base, path):
        """Return a reasonable name for a repo path in the given base."""
        if path.startswith(base + os.path.sep):
            return path.split(base + os.path.sep, 1)[1]
        prefix = os.path.commonprefix([base, path])
        while not base.startswith(prefix + os.path.sep):
            old = prefix
            prefix = os.path.split(prefix)[0]
            if prefix == old:
                break  # Prevent infinite loop, but should be almost impossible
        return path.split(prefix + os.path.sep, 1)[1]

    base = os.path.expanduser(base_path)
    if "max_depth" in kwargs.keys():
        max_depth = kwargs["max_depth"]
        del kwargs["max_depth"]
    else:
        max_depth = 5
    if max_depth >= 0:
        max_depth += 1

    try:
        Repo(base)
        valid = [base]
    except exc.NoSuchPathError:
        paths = glob(base)
        if not paths:
            COUNTER["ERROR"] += 1
            LG.lgr.error(base + " doesn't exist!")
            return
        valid = _collect(paths, max_depth)
    except exc.InvalidGitRepositoryError:
        if not os.path.isdir(base) or max_depth == 0:
            COUNTER["ERROR"] += 1
            LG.lgr.error(base + "isn't a repository!")
            return
        valid = _collect([base], max_depth)

    base = os.path.abspath(base)
    suffix = "" if len(valid) == 1 else "s"
    LG.lgr.info("{fmt}Updating {n} repo{s} at {r}:".format(fmt=RESET + BOLD, r=base,
                                                           n=len(valid), s=suffix))

    valid = [os.path.abspath(path) for path in valid]
    paths = [(_get_basename(base, path), path) for path in valid]
    for name, path in sorted(paths):
        callback(Repo(path), *args, **kwargs)


def update_bookmarks(bookmarks, *args, **kwargs):
    """Loop through and update all bookmarks."""
    if not bookmarks:
        print("You don't have any bookmarks configured! Get help with 'gitup -h'.")
        return

    for path in bookmarks:
        _dispatch(path, _update_repository, *args, **kwargs)


def update_directories(paths, *args, **kwargs):
    """Update a list of directories supplied by command arguments."""
    for path in paths:
        _dispatch(path, _update_repository, *args, **kwargs)


def run_command(paths, command, *args, **kwargs):
    """Run an arbitrary shell command on all repos."""
    for path in paths:
        _dispatch(path, _run_command, command, *args, **kwargs)
