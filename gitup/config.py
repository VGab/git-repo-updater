# -*- coding: utf-8  -*-
#
# Copyright (C) 2011-2016 Ben Kurtovic <ben.kurtovic@gmail.com>
# Released under the terms of the MIT License. See LICENSE for details.

from __future__ import print_function

from glob import glob
import os
import logging
import time
import sys
import coloredlogs
from colorama import Fore, Style
from gitup.migrate import run_migrations

__all__ = ["get_default_config_path", "get_bookmarks", "add_bookmarks",
           "delete_bookmarks", "list_bookmarks", "clean_bookmarks"]

BOLD = Style.BRIGHT
BLUE = Fore.BLUE + BOLD
GREEN = Fore.GREEN + BOLD
RED = Fore.RED + BOLD
YELLOW = Fore.YELLOW + BOLD
RESET = Style.RESET_ALL

INDENT1 = " " * 3
INDENT2 = " " * 6

_WARN = Fore.YELLOW
_OKAY = Fore.GREEN
_ERROR = Fore.RED
_fmt_str = "{fmt}{s}"
_ffmt_str = "{f1}{s}{f2}"


def _fm_print(fmt, s, fmt_end=None):
    if fmt_end is None:
        print(_fmt_str.format(fmt=fmt, s=s))
    else:
        print(_ffmt_str.format(fmt1=fmt, s=s, fmt2=fmt_end))


def _ensure_dirs(path):
    """Ensure the directories within the given pathname exist."""
    dirname = os.path.dirname(path)
    if dirname and not os.path.exists(dirname):  # Race condition, meh...
        os.makedirs(dirname)


def _load_config_file(config_path=None):
    """Read the config file and return a list of bookmarks."""
    run_migrations()
    cfg_path = config_path or get_default_config_path()

    try:
        with open(cfg_path, "rb") as config_file:
            paths = config_file.read().split(b"\n")
    except IOError:
        return []
    paths = [path.decode("utf8").strip() for path in paths]
    return [path for path in paths if path]


def _save_config_file(bookmarks, config_path=None):
    """Save the bookmarks list to the given config file."""
    run_migrations()
    cfg_path = config_path or get_default_config_path()
    _ensure_dirs(cfg_path)

    dump = b"\n".join(path.encode("utf8") for path in bookmarks)
    with open(cfg_path, "wb") as config_file:
        config_file.write(dump)


def _normalize_path(path):
    """Normalize the given path."""
    if path.startswith("~"):
        return os.path.normcase(os.path.normpath(path))
    return os.path.normcase(os.path.abspath(path))


def _get_log_file(file_name='gitup.log'):
    xdg_cfg = os.environ.get("XDG_CONFIG_HOME") or os.path.join("~", ".config")
    fileName = os.path.join(os.path.join(os.path.expanduser(xdg_cfg), "gitup", "logs"),
                            file_name)
    _ensure_dirs(fileName)
    return fileName


class customLogger(object):

    def __init__(self, log_name):
        # setup `logging` module
        self.lgr = logging.getLogger(log_name)
        self.lgr.setLevel(logging.DEBUG)
        format_str = "{levelname:.4}{asctime}  {message}"
        self.formatter = logging.Formatter(format_str, style="{")
        coloredlogs.install(
            logger=self.lgr, fmt=format_str, style="{",
            datefmt="%H:%M:%S",
            level_styles=dict(debug=dict(color='green'), info=dict(color='cyan'),
                              verbose=dict(color='blue'), warning=dict(color='yellow'),
                              error=dict(color='red'), critical=dict(color='red', bold=True)
                              ),
            field_styles=dict(asctime=dict(color='black'), loglevel=dict(color='black', bold=True),
                              hostname=dict(color='magenta'))
        )

    def set_file(self, file_name="gitup.log"):
        _fileName = _get_log_file(file_name)
        msg_str = "{:-^60}".format("> gitup {fmt1}the GIT repo updater{fmt2} <".format(fmt1=Style.RESET_ALL,
                                                                                       fmt2=Fore.CYAN))
        self.lgr.info(msg_str)
        try:
            format_str = "[{levelname:-^4.4}]\t{message}"
            formatter = logging.Formatter(format_str, style="{")
            handler = logging.FileHandler(filename=_fileName, mode='w', encoding='utf8', delay=False)
            handler.setFormatter(formatter)
            handler.setLevel(logging.WARN)
            self.lgr.addHandler(handler)
            self.lgr.info('Errors will be logged to {}'.format(_fileName))
        except:
            self.lgr.exception('\nLogging-Configuration Error\nSkipping logging to file.\n')


LG = customLogger('gitup')


def enable_file_logging():
    LG.set_file()


def get_default_config_path():
    """Return the default path to the configuration file."""
    xdg_cfg = os.environ.get("XDG_CONFIG_HOME") or os.path.join("~", ".config")
    return os.path.join(os.path.expanduser(xdg_cfg), "gitup", "bookmarks")


def get_bookmarks(config_path=None):
    """Get a list of all bookmarks, or an empty list if there are none."""
    return _load_config_file(config_path)


def add_bookmarks(paths, config_path=None):
    """Add a list of paths as bookmarks to the config file."""
    config = _load_config_file(config_path)
    paths = [_normalize_path(path) for path in paths]

    added, exists = [], []
    for path in paths:
        if path in config:
            exists.append(path)
        else:
            config.append(path)
            added.append(path)
    _save_config_file(config, config_path)

    if added:
        _fm_print(fmt=_OKAY + BOLD, s="Added bookmarks:")
        for path in added:
            _fm_print(fmt=INDENT1, s=path)
    if exists:
        _fm_print(fmt=_WARN + BOLD, s="Already marked bookmarks:")
        for path in exists:
            _fm_print(fmt=INDENT1 + _WARN, s=path)


def delete_bookmarks(paths, config_path=None):
    """Remove a list of paths from the bookmark config file."""
    config = _load_config_file(config_path)
    paths = [_normalize_path(path) for path in paths]

    deleted, notmarked = [], []
    if config:
        for path in paths:
            if path in config:
                config.remove(path)
                deleted.append(path)
            else:
                notmarked.append(path)
        _save_config_file(config, config_path)
    else:
        notmarked = paths

    if deleted:
        _fm_print(fmt=_OKAY + BOLD, s="Deleted bookmarks:")
        for path in deleted:
            _fm_print(fmt=INDENT1, s=path)
    if notmarked:
        _fm_print(fmt=_ERROR + BOLD, s="Not bookmarked:")
        for path in notmarked:
            _fm_print(fmt=INDENT1 + _ERROR, s=path)


def list_bookmarks(config_path=None):
    """Print all of our current bookmarks."""
    bookmarks = _load_config_file(config_path)
    if bookmarks:
        _fm_print(fmt=BOLD, s="Current bookmarks:")
        for bookmark_path in bookmarks:
            _fm_print(fmt=INDENT1, s=bookmark_path)
    else:
        LG.lgr.warn("You have no bookmarks to display.")


def clean_bookmarks(config_path=None):
    """Delete any bookmarks that don't exist."""
    bookmarks = _load_config_file(config_path)
    if not bookmarks:
        _fm_print(fmt=_WARN, s="You have no bookmarks to clean up.")
        return

    delete = [path for path in bookmarks
              if not (os.path.isdir(path) or glob(os.path.expanduser(path)))]
    if not delete:
        _fm_print(fmt=_OKAY, s="All of your bookmarks are valid.")
        return

    bookmarks = [path for path in bookmarks if path not in delete]
    _save_config_file(bookmarks, config_path)

    _fm_print(fmt=BOLD + Fore.CYAN, s="Deleted bookmarks:")
    for path in delete:
        _fm_print(fmt=INDENT1, s=path)


def _get_repo_and_branch_from_log_line(l):
    """
    parse information line that contains information as
    "=> Repo <repo_name> (-> Branch <branch_name>): message"
    """
    cur_repo, cur_branch = None, None
    if "=>" in l:
        check = l.split(' ')
        if len(check) > 1:
            cur_repo = check[2]
        if len(check) > 3:
            cur_branch = check[4][:-1]
    return cur_repo, cur_branch


def print_log_file():
    branch_info_str = "{:>60}".format("{b} @ [{g}]")
    repo_info_str = "{:>60}".format("[{g}]")
    color = GREEN
    log_msg = ""
    cur_repo, cur_branch = None, None
    log_file = _get_log_file()
    if not os.path.exists(log_file):
        _fm_print(fmt=_ERROR, s="could not find a log file to skim throug")
        return
    print("\n".join(["-" * 60, "{:#^60}".format("  Gitup - LOG Summary  "), "=" * 60]))
    with open(log_file, 'r') as f:
        for l in f.readlines():
            if "WARN" in l:
                color = _WARN + BOLD
            elif "ERRO" in l or "CRIT" in l:
                color = _ERROR + BOLD
            cur_repo, cur_branch = _get_repo_and_branch_from_log_line(l)
            log_msg += str(l)
            if cur_repo is not None:
                if cur_branch:
                    _fm_print(fmt=color, s=branch_info_str.format(g=cur_repo, b=cur_branch))
                else:
                    _fm_print(fmt=color, s=repo_info_str.format(g=cur_repo))
                print(log_msg)
                log_msg = ""
    if color == GREEN:
        _fm_print(fmt=color, s='Last git update did not encounter any problems')
